<?php
/**
 * Options for odt plugin
 * @author Diego Pierotto <ita.translations@tiscali.it>
 */

// settings must be present and set appropriately for the language
$lang['encoding']   = 'utf-8';
$lang['direction']  = 'ltr';

// export button
$lang['view'] = 'Esporta pagina in formato Open Document';

