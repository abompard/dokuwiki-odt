<?php
/**
 * Italian language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Diego Pierotto <ita.translations@tiscali.it>
 */

// for the configuration manager
$lang['tpl_dir'] = 'Sotto directory dei modelli nel gestore multimediale';
$lang['tpl_default'] = 'Modello predefinito';
