﻿<?php
/**
 * Japanese language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Ikuo Obataya <ikuo_obataya@symplus.co.jp>
 */

// for the configuration manager
$lang['tpl_dir'] = 'テンプレート用サブディレクトリ（media managerで使用）';
