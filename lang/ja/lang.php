﻿<?php
/**
 * Japanese language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Ikuo Obataya <ikuo_obataya@symplus.co.jp>
 */

// settings must be present and set appropriately for the language
$lang['encoding']   = 'utf-8';
$lang['direction']  = 'ltr';

// export button
$lang['view'] = 'このページをOpen Documentフォーマットで保存';

// template not found in the directory
$lang['tpl_not_found'] = '注意：ODTテンプレート "%s" はテンプレートディレクトリ "%s"の中にありませんでした。デフォルトのテンプレートを使用します。';

