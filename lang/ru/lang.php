<?php
/**
 * Russian language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Yuri Timofeev <tim4dev@gmail.com>
 */

// settings must be present and set appropriately for the language
$lang['encoding']   = 'utf-8';
$lang['direction']  = 'ltr';

// export button
$lang['view'] = 'Экспорт страницы в формат Open Document';

// template not found in the directory
$lang['tpl_not_found'] = 'ВНИМАНИЕ: ODT шаблон "%s" не найден в каталоге для шаблонов "%s". Используется шаблон по умолчанию.';

