<?php
/**
 * Chinese language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     lainme <lainme993@gmail.com>
 */

// for the configuration manager
$lang['tpl_dir'] = '用于多媒体管理器模板的子目录';
$lang['tpl_default'] = '默认模板';
